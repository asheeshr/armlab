import lcm
import time

import numpy as np

from lcmtypes import dynamixel_command_t
from lcmtypes import dynamixel_command_list_t
from lcmtypes import dynamixel_status_t
from lcmtypes import dynamixel_status_list_t
from math import *
PI = np.pi
D2R = PI/180.0
R2D = 180.0/PI
ANGLE_TOL = 2*PI/180.0 


""" Rexarm Class """
class Rexarm():
    def __init__(self):

        """ Commanded Values """
        self.joint_angles = [0.0, 0.0, 0.0, 0.0] # radians
        # you SHOULD change this to contorl each joint speed separately 
        self.speed = 0.5                         # 0 to 1
        self.max_torque = 0.5                    # 0 to 1

        self.joint_angles_limits = [(-179*D2R,+180*D2R),(-115*D2R,+115*D2R),(-115*D2R,+115*D2R),(-115*D2R,+115*D2R)]

        """ Feedback Values """
        self.joint_angles_fb = [0.0, 0.0, 0.0, 0.0] # radians
        self.speed_fb = [0.0, 0.0, 0.0, 0.0]        # 0 to 1   
        self.load_fb = [0.0, 0.0, 0.0, 0.0]         # -1 to 1  
        self.temp_fb = [0.0, 0.0, 0.0, 0.0]         # Celsius  

        """ Forward Kinematics"""
        self.dh = [[0,0,11.5,PI/2],[0,0,10,0],[0,0,10,0],[0,0,11,0]]
        self.check = [[0,0,11.5,PI/2],[0,0,10,0],[0,0,10,0],[0,0,11,0]]
        self.link = [0,0,0,0]
        self.A = [0,0,0,0]
        self.tool_world_coords = [0,0,0,0] #x,y,z,alpha


        """ Plan - TO BE USED LATER """
        self.plan = []
        self.plan_status = 0
        self.wpt_number = 0
        self.wpt_total = 0
        self.speeds = []
        self.endEffector = []
        self.actualPoints = []
        self.init = False
	self.joints_commanded = []

        """ LCM Stuff"""
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("ARM_STATUS",
                                        self.feedback_handler)

    def cmd_publish(self):
        """ 
        Publish the commands to the arm using LCM. 
        NO NEED TO CHANGE.
        You need to activelly call this function to command the arm.
        You can uncomment the print statement to check commanded values.
        """    
        msg = dynamixel_command_list_t()
        msg.len = 4
        
	self.joints_commanded.append((time.time(), self.joint_angles[0], self.joint_angles[1], self.joint_angles[2], self.joint_angles[3]))
	joints_commanded = np.asarray(self.joints_commanded)
        np.savetxt('joint_commanded.dat',joints_commanded, delimiter=',')
	self.clamp()
	for i in range(msg.len):
            cmd = dynamixel_command_t()
            cmd.utime = int(time.time() * 1e6)
            cmd.position_radians = self.joint_angles[i]
            # you SHOULD change this to contorl each joint speed separately 
            cmd.speed = self.speed
            cmd.max_torque = self.max_torque
            # print cmd.position_radians
            msg.commands.append(cmd)
        self.lc.publish("ARM_COMMAND",msg.encode())
    
    def get_feedback(self):
        """
        LCM Handler function
        Called continuously from the GUI 
        NO NEED TO CHANGE
        """
        self.lc.handle_timeout(50)

    def feedback_handler(self, channel, data):
        """
        Feedback Handler for LCM
        NO NEED TO CHANGE FOR NOW.
        LATER NEED TO CHANGE TO MANAGE PLAYBACK FUNCTION 
        """
        msg = dynamixel_status_list_t.decode(data)
        angleSum = 0

        if (not self.init):
            self.baseTime = int(msg.statuses[0].utime)
            self.init = True


        for i in range(msg.len):
            self.joint_angles_fb[i] = msg.statuses[i].position_radians
            if (i == 0):
                self.dh[i][3] = self.joint_angles_fb[i]
            else:
                angleSum += self.joint_angles_fb[i]
                self.dh[i][0] = self.joint_angles_fb[i] 
            self.speed_fb[i] = msg.statuses[i].speed 
            self.load_fb[i] = msg.statuses[i].load 
            self.temp_fb[i] = msg.statuses[i].temperature

        tmp = [0,0,0,1]

        tmp = self.rexarm_FK(self.dh,3,tmp)
        tmp = self.rexarm_FK(self.dh,2,tmp)
        tmp = self.rexarm_FK(self.dh,1,tmp)
        self.tool_world_coords = self.rexarm_FK(self.dh,0,tmp)
        self.tool_world_coords[3] = 3*PI/2.0 - angleSum
        if (self.tool_world_coords[3] > PI):
            self.tool_world_coords[3] = -1*(2*PI-self.tool_world_coords[3])
        elif (self.tool_world_coords[3] < -PI):
            self.tool_world_coords[3] = 2*PI + self.tool_world_coords[3]
        


        self.actualPoints.append((int(msg.statuses[0].utime)- self.baseTime,self.joint_angles_fb[0],self.joint_angles_fb[1],self.joint_angles_fb[2],self.joint_angles_fb[3]))
        self.speeds.append((int(msg.statuses[0].utime)- self.baseTime,self.speed_fb[0], self.speed_fb[1],self.speed_fb[2],self.speed_fb[3]))
        self.endEffector.append((int(msg.statuses[0].utime)- self.baseTime,self.tool_world_coords[0],self.tool_world_coords[1],self.tool_world_coords[2],self.tool_world_coords[3]))

        joints = np.asarray(self.actualPoints)
        np.savetxt('joint_positions.dat',joints, delimiter=',')
        speeds = np.asarray(self.speeds)
        #np.savetxt('speeds.dat',speeds, delimiter=',')
        endEffector = np.asarray(self.endEffector)
        #np.savetxt('end_effector.dat',endEffector, delimiter=',')


    def clamp(self):
        """
        Clamp Function
        Limit the commanded joint angles to ones physically possible so the 
        arm is not damaged.
        TO DO: IMPLEMENT SUCH FUNCTION
        """

	print "Clamp Function!"
        for i in range(0,4):
            if self.joint_angles[i]<self.joint_angles_limits[i][0]:
                self.joint_angles[i] = self.joint_angles_limits[i][0]
            if self.joint_angles[i]>self.joint_angles_limits[i][1]:
                self.joint_angles[i] = self.joint_angles_limits[i][1]
        

    def plan_command(self):
        """ Command waypoints - TO BE ADDED LATER """
        pass

    def rexarm_FK(self,dh_table, link, coords):
        """
        Calculates forward kinematics for rexarm
        takes a DH table filled with DH parameters of the arm
        and the link to return the position for
        returns a 4-tuple (x, y, z, phi) representing the pose of the 
        desired link
        """
        
        dh = dh_table[link]
        #self.A[link] = create_Rz(dh)
       # print(self.create_Rz(dh))
        #print dh_table
        return np.dot(self.create_Rz(dh), np.transpose(coords))
    
    def create_Rz(self,dh):
        Rz = np.identity(4)
        Rx = np.identity(4)
        Tz = np.identity(4)
        Tx = np.identity(4)

        Rz[0][0] = cos(dh[0])
        Rz[0][1] = -sin(dh[0])
        Rz[1][0] = sin(dh[0])
        Rz[1][1] = cos(dh[0])

        Rx[1][1] = cos(dh[3])
        Rx[1][2] = -sin(dh[3])
        Rx[2][1] = sin(dh[3])
        Rx[2][2] = cos(dh[3])

        Tz[2][3] = dh[1]

        Tx[0][3] = dh[2]

        RzTz = np.dot(Rz,Tz)
        RzTzTx = np.dot(RzTz,Tx)
        return np.dot(RzTzTx,Rx)



    def rexarm_IK(self,pose, cfg):
        """
        Calculates inverse kinematics for the rexarm
        pose is a tuple (x, y, z, phi) which describes the desired
        end effector position and orientation.  
        cfg describe elbow down (0) or elbow up (1) configuration
        returns a 4-tuple of joint angles or NONE if configuration is impossible
        """

        totalDistance = sqrt(pow(pose[0],2) + pow(pose[1],2) + pow(pose[2],2))
        if (not self.check_valid_position(pose)):
            print "Impossible configuration"
            return []
        angles = [0,0,0,0]
        angles[0] = atan2(-1*pose[2], -1*pose[1])
        d = sqrt(pow(pose[2],2) + pow(pose[1],2))
        newPose = (pose[0],d)
        translatedPose = (newPose[0]+11*sin(pose[3]*D2R),newPose[1]-11*cos(pose[3]*D2R))
       # print str(translatedPose) + " " + str(newPose)
        d = sqrt(pow(translatedPose[0]-11.5,2)+pow(translatedPose[1],2))
        #print str(translatedPose[0]) + " " + str(translatedPose[1])
        #print str(newPose) + " " + str(translatedPose) + " " + str(d)

        jointLength = 10
        if (d > jointLength*2):
            print "Impossible configuration : translatedPose"
            print "jointLength", jointLength
            return []


        eLength = d
        sLength = 10
        wLength = 10
        sAngle = acos((pow(eLength,2) + pow(wLength,2) - pow(sLength,2))/(2*eLength * wLength))
        eAngle = acos((pow(sLength,2) + pow(wLength,2) - pow(eLength,2))/(2*sLength * wLength))
        wAngle = acos((pow(eLength,2) + pow(sLength,2) - pow(wLength,2))/(2*eLength * sLength))
        #print str(sAngle) +" " + str(eAngle) +" "+ str(wAngle) + " " + str(sAngle + eAngle + wAngle)
        #print str((-PI/4-sAngle)*R2D) 
        littleAngle = atan2(translatedPose[0]-11.5,translatedPose[1])

        if (cfg == 1):
            angles[1] = -(PI/2-sAngle-littleAngle)
            angles[2] = -(-PI-eAngle)

        if (cfg == 0):
            angles[1] = -(PI/2-sAngle-littleAngle + 2*sAngle)
            angles[2] = (-PI-eAngle)

        for i in range(3):
            if ((angles[i]) > PI):
                angles[i] = -(2*PI - angles[i])
            elif (angles[i] < -PI):
                angles[i] = 2*PI + angles[i]

        angles[3] = (-pose[3]*D2R + 3*PI/2 - angles[1] - angles[2])
        
        if ((angles[3]) > PI):
                angles[3] = -(2*PI - angles[3])
        elif (angles[3] < -PI):
                angles[3] = 2*PI + angles[3]
             
        #for i in range(4):
        #    print angles[i]*R2D,
        #print
        return (angles) #base, shoulder, elbow, wrist
        
    def rexarm_collision_check(self,joints):
        """
        Perform a collision check with the ground and the base
        takes a 4-tuple of joint angles q
        returns true if no collision occurs
        """
        angleSum = 0
        for i in range(4):
            if (i == 0):
                self.check[i][3] = joints[i]
            else:
                angleSum += joints[i]
                self.check[i][0] = joints[i] 

        #print self.dh
        tmp = [0,0,0,1]

        tmp = self.rexarm_FK(self.check,3,tmp)
        tmp = self.rexarm_FK(self.check,2,tmp)
        tmp = self.rexarm_FK(self.check,1,tmp)
        pose = self.rexarm_FK(self.check,0,tmp)
        pose[3] = 3*PI/2.0 - angleSum
        if (pose[3] > PI):
            pose[3] = -1*(2*PI-pose[3])
        elif (pose[3] < -PI):
            pose[3] = 2*PI + pose[3]

        if (pose[2] > 0):
            cfg = 1
        else: 
            cfg = 0

        if (pose[0] < 1):
            pose[0] = 3
            print "Fixed"
        else: 
            return joints

        return self.rexarm_IK(pose,cfg)


    def check_valid_position(self,pose):
        totalDistance = sqrt(pow(pose[0],2) + pow(pose[1],2) + pow(pose[2],2))
        if (totalDistance > 43):
            print "Impossible configuration : totalDistance exceeds limit"
            return False

        if (pose[0] < 0):
            print "Impossible configuration : can't dig"
            return False
            '''
        if (pose[0] > 11.5):
            totalDistance = sqrt(pow(pose[0]-11.5,2) + pow(pose[1],2) + pow(pose[2],2))
            if (totalDistance > 31):
                return False

        if (pose[0] < 11.5):
            if (pose[1] == 0 and pose[2] == 0):
                return False
                '''
        return True
