import sys
import cv2
import numpy as np
from scipy.cluster.vq import kmeans
from PyQt4 import QtGui, QtCore, Qt
from ui import Ui_MainWindow
from rexarm import Rexarm
from numpy.linalg import inv,norm
import time
from video import Video
from math import *
import matplotlib.pyplot as plt

""" Radians to/from  Degrees conversions """
D2R = 3.141592/180.0
R2D = 180.0/3.141592
PI = np.pi

""" Pyxel Positions of image in GUI """
MIN_X = 310
MAX_X = 950

MIN_Y = 30
MAX_Y = 510

class Gui(QtGui.QMainWindow):
    """ 
    Main GUI Class
    It contains the main function and interfaces between 
    the GUI and functions
    """
    def __init__(self,parent=None):

        self.tr_init = False
        QtGui.QWidget.__init__(self,parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        """ Main Variables Using Other Classes"""
        self.rex = Rexarm()
        self.video = Video(cv2.VideoCapture(0))

        """ Other Variables """
        self.last_click = np.float32([0,0])

        """ Set GUI to track mouse """
        QtGui.QWidget.setMouseTracking(self,True)

        """ 
        Video Function 
        Creates a timer and calls play() function 
        according to the given time delay (27mm) 
        """
        self._timer = QtCore.QTimer(self)
        self._timer.timeout.connect(self.play)
        self._timer.start(5)
       
        """ 
        Playback Function 
        Creates a timer and calls playback function 
        according to the given time delay 
        """
        self._timer3 = QtCore.QTimer(self)
        self._timer3.timeout.connect(self.tr_playback)
       


        """ 
        LCM Arm Feedback
        Creates a timer to call LCM handler continuously
        No delay implemented. Reads all time 
        """  
        self._timer2 = QtCore.QTimer(self)
        self._timer2.timeout.connect(self.rex.get_feedback)
        self._timer2.start()

        """ 
        Connect Sliders to Function
        TO DO: CONNECT THE OTHER 5 SLIDERS IMPLEMENTED IN THE GUI 
        """ 
        self.ui.sldrBase.valueChanged.connect(self.slider_change)
        self.ui.sldrShoulder.valueChanged.connect(self.slider_change)
        self.ui.sldrElbow.valueChanged.connect(self.slider_change)
        self.ui.sldrWrist.valueChanged.connect(self.slider_change)
        self.ui.sldrMaxTorque.valueChanged.connect(self.slider_change)
        self.ui.sldrSpeed.valueChanged.connect(self.slider_change)

        """ Commands the arm as the arm initialize to 0,0,0,0 angles """
        self.slider_change() 
        
        """ Connect Buttons to Functions """
        self.ui.btnLoadCameraCal.clicked.connect(self.load_camera_cal)
        self.ui.btnPerfAffineCal.clicked.connect(self.affine_cal)
        self.ui.btnTeachRepeat.clicked.connect(self.tr_initialize)
        self.ui.btnAddWaypoint.clicked.connect(self.tr_add_waypoint)
        self.ui.btnSmoothPath.clicked.connect(self.tr_smooth_path)
        self.ui.btnPlayback.clicked.connect(self.tr_playback)
        self.ui.btnDefineTemplate.clicked.connect(self.def_template)
        self.ui.btnLocateTargets.clicked.connect(self.template_match)
        self.ui.btnExecutePath.clicked.connect(self.exec_path)
        self.x_world = 0
        self.y_world = 0
        self.wypt_list = []
        self.wypt_list_playback = []
        self.first_time_stamp = 0
        self.wypt_index = 0
        self.cmmd_published = False
        self.tr_playback_started = False
        self.new_path = []
        self.using_new_path = False
        self.target_points = []
        self.template_coordinates = []
        self.template_coordinates_defined = False

    def play(self):
        """ 
        Play Funtion
        Continuously called by GUI 
        """

        """ Renders the Video Frame """
        try:
            self.video.captureNextFrame()
            self.ui.videoFrame.setPixmap(
                self.video.convertFrame())
            self.ui.videoFrame.setScaledContents(True)

            if self.template_coordinates_defined:
                #Extract template
                print "Template co-ordinates", self.video.mouse_coord
                temp = cv2.resize(self.video.currentFrame,(640,480))
                template_img = temp[self.video.mouse_coord[0][1]:self.video.mouse_coord[1][1], self.video.mouse_coord[0][0]:self.video.mouse_coord[1][0]]
                template_img = cv2.cvtColor(template_img, cv2.COLOR_BGR2GRAY)
                cv2.imwrite("template.png", template_img)
                cv2.imshow("template", template_img)
                cv2.waitKey(0)
                
                self.template_coordinates_defined = False

        except TypeError:
            print "No frame"
        
        """ 
        Update GUI Joint Coordinates Labels
        TO DO: include the other slider labels 
        """
        self.ui.rdoutBaseJC.setText(str(self.rex.joint_angles_fb[0]*R2D))
        self.ui.rdoutShoulderJC.setText(str(self.rex.joint_angles_fb[1]*R2D))
        self.ui.rdoutElbowJC.setText(str(self.rex.joint_angles_fb[2]*R2D))
        self.ui.rdoutWristJC.setText(str(self.rex.joint_angles_fb[3]*R2D))

        #WorldCoords
        self.ui.rdoutX.setText(str(self.rex.tool_world_coords[0]))
        self.ui.rdoutY.setText(str(self.rex.tool_world_coords[1]))
        self.ui.rdoutZ.setText(str(self.rex.tool_world_coords[2]))
        self.ui.rdoutT.setText(str(self.rex.tool_world_coords[3]*R2D))


        """ 
        Mouse position presentation in GUI
        TO DO: after getting affine calibration make the apprriate label
        to present the value of mouse position in world coordinates 
        """    
        x = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).x()
        y = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).y()
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)):
            self.ui.rdoutMousePixels.setText("(-,-)")
            self.ui.rdoutMouseWorld.setText("(-,-)")
        else:
            x = x - MIN_X
            y = y - MIN_Y
            self.ui.rdoutMousePixels.setText("(%.0f,%.0f)" % (x,y))
            if (self.video.aff_flag == 2):
                """ TO DO Here is where affine calibration must be used """
                tmp = self.video.aff_matrix

                self.x_world = tmp[0][0]*x + tmp[0][1]*y+tmp[0][2]
                self.y_world = tmp[1][0]*x + tmp[1][1]*y+tmp[1][2]

                self.ui.rdoutMouseWorld.setText("("+str(int(self.x_world))+","+str(int(self.y_world))+")")
            else:
                self.ui.rdoutMouseWorld.setText("(-,-)")

        """ 
        Updates status label when rexarm playback is been executed.
        This will be extended to includ eother appropriate messages
        """ 
        if(self.rex.plan_status == 1):
            self.ui.rdoutStatus.setText("Playing Back - Waypoint %d"
                                    %(self.rex.wpt_number + 1))


    def slider_change(self):
        """ 
        Function to change the slider labels when sliders are moved
        and to command the arm to the given position 
        TO DO: Implement for the other sliders
        """
        self.ui.rdoutBase.setText(str(self.ui.sldrBase.value()))
        self.ui.rdoutTorq.setText(str(self.ui.sldrMaxTorque.value()) + "%")
        self.ui.rdoutSpeed.setText(str(self.ui.sldrSpeed.value()) + "%")

        self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0
        self.rex.joint_angles[0] = self.ui.sldrBase.value()*D2R
        
        self.ui.rdoutShoulder.setText(str(self.ui.sldrShoulder.value()))
        self.rex.joint_angles[1] = self.ui.sldrShoulder.value()*D2R
        
        self.ui.rdoutElbow.setText(str(self.ui.sldrElbow.value()))
        self.rex.joint_angles[2] = self.ui.sldrElbow.value()*D2R
        
        self.ui.rdoutWrist.setText(str(self.ui.sldrWrist.value()))
        self.rex.joint_angles[3] = self.ui.sldrWrist.value()*D2R
        
        self.rex.speed = self.ui.sldrSpeed.value()/100.0

        if (self.tr_init == False):
            self.rex.cmd_publish()

    def mousePressEvent(self, QMouseEvent):
        """ 
        Function used to record mouse click positions for 
        affine calibration 
        """
 
        """ Get mouse posiiton """
        x = QMouseEvent.x()
        y = QMouseEvent.y()

        """ If mouse position is not over the camera image ignore """
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)): return

        """ Change coordinates to image axis """
        self.last_click[0] = x - MIN_X
        self.last_click[1] = y - MIN_Y
       
        """ If affine calibration is been performed """
        if (self.video.aff_flag == 1):
            """ Save last mouse coordinate """
            self.video.mouse_coord[self.video.mouse_click_id] = [(x-MIN_X),
                                                                 (y-MIN_Y)]

            if self.video.mouse_click_id == 0:
                np.savetxt('center.dat',self.last_click, delimiter=',')


            """ Update the number of used poitns for calibration """
            self.video.mouse_click_id += 1

            """ Update status label text """
            self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                      %(self.video.mouse_click_id + 1))

            """ 
            If the number of click is equal to the expected number of points
            computes the affine calibration.
            TO DO: Change this code to use you programmed affine calibration
            and NOT openCV pre-programmed function as it is done now.
            """
            if(self.video.mouse_click_id == self.video.aff_npoints):
                """ 
                Update status of calibration flag and number of mouse
                clicks
                """
                self.video.aff_flag = 2
                self.video.mouse_click_id = 0
                
                # """ Perform affine calibration with OpenCV """
                # self.video.aff_matrix = cv2.getAffineTransform(
                #                         self.video.mouse_coord,
                #                         self.video.real_coord)


                # print 'opencv',self.video.aff_matrix

                """affine calibration"""
                mouse_pts = self.video.mouse_coord
                real_pts = self.video.real_coord

                A_matrix = [[mouse_pts[0][0], mouse_pts[0][1], 1, 0, 0, 0], \
                [0, 0, 0, mouse_pts[0][0], mouse_pts[0][1], 1], \
                [mouse_pts[1][0], mouse_pts[1][1], 1, 0, 0, 0],\
                [0, 0, 0, mouse_pts[1][0], mouse_pts[1][1], 1], \
                [mouse_pts[2][0], mouse_pts[2][1], 1, 0, 0, 0],\
                [0, 0, 0, mouse_pts[2][0], mouse_pts[2][1], 1],\
                [mouse_pts[3][0], mouse_pts[3][1], 1, 0, 0, 0],\
                [0, 0, 0, mouse_pts[3][0], mouse_pts[3][1], 1], \
                [mouse_pts[4][0], mouse_pts[4][1], 1, 0, 0, 0],\
                [0, 0, 0, mouse_pts[4][0], mouse_pts[4][1], 1]]

                b_vec = [[real_pts[0][0]],[real_pts[0][1]],[real_pts[1][0]],[real_pts[1][1]],\
                [real_pts[2][0]],[real_pts[2][1]],[real_pts[3][0]],[real_pts[3][1]],[real_pts[4][0]],[real_pts[4][1]]]

                A_matrix = np.asmatrix(A_matrix)

                b_vec = np.asarray(b_vec)

                x_vec = np.dot(np.dot(inv(np.dot(np.transpose(A_matrix),A_matrix)),np.transpose(A_matrix)),b_vec)

                self.video.aff_matrix = [[x_vec[0],x_vec[1],x_vec[2]], [x_vec[3],x_vec[4],x_vec[5]]]

                tmp = np.asarray(self.video.aff_matrix)
                np.savetxt('aff_matrix.dat',tmp, delimiter=',')

                """ Updates Status Label to inform calibration is done """ 
                self.ui.rdoutStatus.setText("Waiting for input")

                """ 
                Uncomment to gether affine calibration matrix numbers 
                on terminal
                """ 
                print 'self-defined:',self.video.aff_matrix

        if (self.video.template_flag == 1):
            """ Save last mouse coordinate """
            self.video.mouse_coord[self.video.mouse_click_id] = [(x-MIN_X),
                                                                 (y-MIN_Y)]

            """ Update the number of used poitns for calibration """
            self.video.mouse_click_id += 1

            """ Update status label text """
            self.ui.rdoutStatus.setText("Template Definition: Click point %d" 
                                      %(self.video.mouse_click_id + 1))

            """ 
            If the number of click is equal to the expected number of points
            computes the affine calibration.
            TO DO: Change this code to use you programmed affine calibration
            and NOT openCV pre-programmed function as it is done now.
            """
            if(self.video.mouse_click_id == self.video.template_npoints):
                """ 
                Update status of calibration flag and number of mouse
                clicks
                """
                self.video.template_flag = 2
                self.video.mouse_click_id = 0
                self.template_coordinates_defined = True

                """template definition"""
                mouse_pts = self.video.mouse_coord
                real_pts = self.video.real_coord

                """ Updates Status Label to inform calibration is done """ 
                self.ui.rdoutStatus.setText("Waiting for input")

                """ 
                Uncomment to gether affine calibration matrix numbers 
                on terminal
                """ 
                print 'self-defined:'


    def affine_cal(self):
        """ 
        Function called when affine calibration button is called.
        Note it only chnage the flag to record the next mouse clicks
        and updates the status text label 
        """
        self.video.aff_flag = 1 
        self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                    %(self.video.mouse_click_id + 1))

    def load_camera_cal(self):
        print "Load Camera Cal"
        self.video.loadCalibration()
        self.video.aff_matrix = np.loadtxt('aff_matrix.dat',delimiter=',')
        self.video.aff_flag = 2



    def tr_initialize(self):

        print "Teach and Repeat"
        self.tr_init = True
        self.rex.max_torque = 0.0
        self.rex.cmd_publish()

        self.first_time_stamp = int(time.time()*1000.0)


    def tr_add_waypoint(self):
        print "Add Waypoint"
        element_tmp = [int(int(time.time()*1000.0)-self.first_time_stamp)]
        element_tmp.extend([i for i in self.rex.joint_angles_fb])
        element_tmp.extend([i*1023*0.114*2*3.14/60 for i in self.rex.speed_fb])
        self.wypt_list.append(element_tmp)
        tmp = np.asarray(self.wypt_list)
        # print 'wypt_list',self.wypt_list
        np.savetxt('waypoint_list.dat',tmp, delimiter=',')


    def tr_smooth_path_linear(self):

        i_num = 4.0 #number of segments between two way points
        self.new_path = []

        self.using_new_path = True
        targets = np.loadtxt('waypoint_list.dat',delimiter=',')
        self.new_path.append((0, 0, 0, 0, 0))

        prev_wypt = (0, 0, 0, 0, 0)
        for curr_target in targets:
            step = ((curr_target[0] - prev_wypt[0])/(i_num), (curr_target[1] - prev_wypt[1])/(i_num), (curr_target[2] - prev_wypt[2])/(i_num), \
                (curr_target[3] - prev_wypt[3])/(i_num), (curr_target[4] - prev_wypt[4])/(i_num))
            print 'step', step
            for i in range(int(i_num)-1):
                tmp = [ (prev_wypt[j]+step[j]*i) for j in range(5)]
                self.new_path.append(tuple(tmp))
            prev_wypt = curr_target

        self.using_new_path = True
        tmp = 200
        self.new_path = [(tmp+i*200, self.new_path[i][1], self.new_path[i][2], self.new_path[i][3], self.new_path[i][4]) for i in range(len(self.new_path))]
        print self.new_path

        print "Smooth Path"

    def tr_smooth_path(self):

        self.new_path = []
        self.using_new_path = True
        targets = np.loadtxt('waypoint_list.dat',delimiter=',')
        # self.new_path.append((time.time(),0,0,0,0,0,0,0,0)) #Time, 4 Joint Angles, 4 Joint Speeds

        coeff_matrix = [[0 for i in range(4)] for j in range(4)]

        prev_target = targets[0]
        self.new_path.append(prev_target)
        cout = 0
        for curr_target in targets:
            if cout == 0:
                cout = 1
                continue

            delta_t = (curr_target[0]-prev_target[0])/1000.0
            for j in range(4):
                q_f = curr_target[j+1]
                q_o = prev_target[j+1]
                v_o = prev_target[j+1+4]
                v_f = curr_target[j+1+4]
    
                coeff_matrix[j][0] = q_o
                coeff_matrix[j][1] = v_o
                coeff_matrix[j][2] = (3*(q_f - q_o) - (2*v_o+v_f)*(delta_t))/(delta_t*delta_t)
                coeff_matrix[j][3] = (2*(q_o - q_f) + (v_o + v_f)*(delta_t))/(delta_t*delta_t*delta_t)

            steps = int(max([abs(curr_target[i+1]-prev_target[i+1]) for i in range(4)])/(2*D2R))
            if steps == 0:
                steps = 1
            #steps = 2
            #print 'step',steps
            time_step = delta_t/float(steps)


            for i in range(1,steps+1):
                waypoint = [prev_target[0]+time_step*i]
                time_array = [1, time_step*i, pow(time_step*i,2), pow(time_step*i,3)]
                waypoint.extend(np.dot(time_array,np.transpose(coeff_matrix)))
                time_array = [0, 1, 2*(time_step*i), 3*(pow(time_step*i,2))]
                waypoint.extend(np.dot(time_array,np.transpose(coeff_matrix)))
                if (self.rex.rexarm_collision_check(waypoint[1:5]) is not []):
                    self.new_path.append(tuple(waypoint))   
                    
            
            
            #print "Current waypoint", curr_target
            #print "prev_target", prev_target
            #print "coeff_matrix", coeff_matrix
            #input("Waiting for input")
            prev_target = curr_target
        # for i in self.new_path:
        #     for j in i:
        #         print j, ",",
        #     print ""
        #self.new_path = []
        np.savetxt('smoothed_waypoint_list.dat', self.new_path, delimiter=',')
        
        print "Smooth Path"

    def tr_playback(self):
        # print "Playback"
        #print self.using_new_path
        if self.tr_playback_started == False:
            self.start_time_playback = int(time.time()*1000.0)
            if(self.using_new_path):
                self.wypt_list_playback = self.new_path
            else:
                self.wypt_list_playback = np.loadtxt('waypoint_list.dat',delimiter=',')
            self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0
            self.err = np.array([0.0, 0.0, 0.0, 0.0])
            self.rex.speed = self.ui.sldrSpeed.value()/100.0
            self.wypt_index = 0
            self.tr_playback_started = True
            self._timer3.start(30)
            self.waypoints_covered = []


        if self.wypt_index == len(self.wypt_list_playback):
            self._timer3.stop()
            print " Timer stopped"
            self.tr_playback_started = False
            #Output waypoint list to file
            tmp = np.asarray(self.waypoints_covered)
            np.savetxt('waypoint_covered_list.dat',tmp, delimiter=',')
            self.using_new_path = False
            return

        for i in range(4):
            self.rex.joint_angles[i] = self.wypt_list_playback[self.wypt_index][i+1]
            self.err[i] = self.rex.joint_angles[i] - self.rex.joint_angles_fb[i]
        if self.cmmd_published == False:
            self.cmd_time = int(time.time()*1000.0) 
            self.rex.cmd_publish()
            self.cmmd_published = True
          # print 'err',norm(err)

          #TO INCREASE SPEED - CHANGE THE DIVISOR HERE --------------------------------------------------------------------------------->
        #print "Error", norm(self.err)
        if (norm(self.err) < 3.0) and (int(time.time()*1000.0) - self.start_time_playback >= self.wypt_list_playback[self.wypt_index][0]):
            self.wypt_index += 1
            element_tmp = [int(int(time.time()*1000.0)-self.first_time_stamp)]
            element_tmp.extend([i for i in self.rex.joint_angles_fb])
            self.waypoints_covered.append(element_tmp)
            self.cmmd_published = False

    def generate_path(self):
        self.new_path = []
        self.using_new_path = True
        targets = np.loadtxt('waypoint_list.dat',delimiter=',')
        self.new_path.append((0, 0, 0, 0, 0))
        time_step = 1000
        time_tmp = 2000

        for curr_target in targets:
            time_tmp += time_step
            self.new_path.append((time_tmp, curr_target[1], curr_target[2]/3.0, curr_target[3]/3.0, curr_target[4]/3.0))

            time_tmp += abs(time_step*curr_target[1]/3.1415)
            self.new_path.append((time_tmp, curr_target[1], 2.0*curr_target[2]/3.0, curr_target[3]/3.0, curr_target[4]/3.0))

            time_tmp += abs(time_step*curr_target[3]/3.1415)
            self.new_path.append((time_tmp, curr_target[1], 2.0*curr_target[2]/3.0, curr_target[3]/3.0, curr_target[4]))

            time_tmp += abs(time_step*curr_target[4]/3.1415)
            self.new_path.append((time_tmp, curr_target[1], 2.0*curr_target[2]/3.0, 2.0*curr_target[3]/3.0, 1.1*curr_target[4]))
            #self.new_path.append((time_tmp, curr_target[1], curr_target[2], 0, curr_target[4]))
            time_tmp += abs(time_step*curr_target[3]/3.1415)+1000
            self.new_path.append((time_tmp, curr_target[1], curr_target[2], curr_target[3], curr_target[4]))

            # pick up
            time_tmp += time_step+1000
            # self.new_path.append((time_tmp, curr_target[1], 2.0*curr_target[2]/3.0, curr_target[3], curr_target[4]))
            # time_tmp += abs(time_step*)
            self.new_path.append((time_tmp, curr_target[1], 2.0*curr_target[2]/3.0, 2.0*curr_target[3]/3.0, 1.1*curr_target[4]))

            time_tmp += abs(time_step*curr_target[3]/3.1415)
            self.new_path.append((time_tmp, curr_target[1], 2.0*curr_target[2]/3.0, curr_target[3]/3.0, curr_target[4]))
            time_tmp += abs(time_step*curr_target[4]/3.1415)
            self.new_path.append((time_tmp, curr_target[1], 2.0*curr_target[2]/3.0, curr_target[3]/3.0, curr_target[4]/3.0))
            time_tmp += abs(time_step*curr_target[2]/3.1415)
            self.new_path.append((time_tmp, curr_target[1], curr_target[2]/3.0, curr_target[3]/3.0, curr_target[4]/3.0))
        np.savetxt('smoothed_waypoint_list.dat', self.new_path, delimiter=',')
        

    def def_template(self):
        #video = Video(cv2.VideoCapture(0))
        #self.video.captureNextFrame()
        #self.video.capture.grab()
        #retval,image = self.video.capture.retrieve()
        self.video.template_flag = True

    def makeSin(self):
        x = 0
        y = 0
        z = 0

        for t in range (1,20000, 500):
            x = 5*sin(2*PI * t/10000.0) + 15
            y = -20#-5*sin(2*PI * t/10000.0) - 22.5
            z = t/500.0 - 20
            self.target_points.append((x,y,z,t)) 

        for t in range (20000,40000, 500):
            x = 5*sin(2*PI * t/10000.0) + 15
            y = -20#5*sin(2*PI * t/10000.0) - 22.5
            z = 40 - (t/500.0 - 20)
            self.target_points.append((x,y,z,t)) 

        waypoint_list = []
        for member in self.target_points:
            pose = (member[0],member[1],member[2],0)
            joint_angles = self.rex.rexarm_IK(pose,1)
            if (joint_angles != [] ):
                waypoint_list.append((member[3],joint_angles[0],joint_angles[1],
                    joint_angles[2],joint_angles[3],0.2,0.2,0.2,0.2))

       # print waypoint_list
        tmp = np.asarray(waypoint_list)
        np.savetxt('waypoint_list.dat',tmp, delimiter=',')

        print "Define Template"

    def match_preprocess(self, currentFrame):


        wframe, hframe = currentFrame.shape[::-1]
        center = np.loadtxt('center.dat',delimiter=',')
        
        image_mask = np.zeros((hframe, wframe), np.uint8)
        image_mask[:] = 255
        #cv2.circle(image_mask, (wframe/2, hframe/2), 180, 0, thickness=-1)
        cv2.circle(image_mask, (int(center[0]+10),int(center[1])-10), 160, 0, thickness=-1)
        cv2.rectangle(image_mask, (int(center[0]-12),int(center[1])+12), (int(center[0])+36,int(center[1])-36), 255, thickness=-1)
        currentFrame = cv2.bitwise_or(currentFrame, image_mask)

        hist = cv2.calcHist([currentFrame],[0],None,[256],[0,256])

        sum_thresh = 0
        j = 0
        for i in hist:
            j += 1
            sum_thresh += i
            if sum_thresh/sum(hist)>.012:
                break
        hist_thresh = j
        hist_thresh = 80
        '''
        counter = 0
        hist_flag = False
        for i in range(len(hist)):
            counter+=1
            if not (( i < sum(hist[:128])/32) and hist_flag == False):
                print "Low"
                hist_flag = True
            if (( i < sum(hist[:128])/32) and hist_flag == True):
                print "Second Low"
                break
        hist_thresh = counter 
        print "HistThresh",hist_thresh
        '''

        ret, currentFrame = cv2.threshold(currentFrame, hist_thresh, 255, cv2.THRESH_BINARY)
        template = cv2.imread('template.png',cv2.IMREAD_GRAYSCALE)
        ret, template = cv2.threshold(template, hist_thresh, 255, cv2.THRESH_BINARY)

        cv2.imwrite("preprocessed-frame.png", currentFrame)
        cv2.imwrite("preprocessed-template.png", template)

    def match(self):

        center = np.loadtxt('center.dat',delimiter=',')
        template_processed = cv2.imread('preprocessed-template.png',cv2.IMREAD_GRAYSCALE)
        currentFrame_processed = cv2.imread('preprocessed-frame.png',cv2.IMREAD_GRAYSCALE)

        w_proc, h_proc = currentFrame_processed.shape[::-1]
        print "Size of proc frame", w_proc, h_proc

        print " Center", center, int(center[0])-170/2, int(center[0])+170/2, int(center[1])-170/2, int(center[1])+170/2
        subimage = currentFrame_processed[int(center[1])-170:int(center[1])+170,int(center[0])-170:int(center[0])+170]
        cv2.imwrite("preprocessed-subimage.png", subimage)

        w, h = subimage.shape[::-1]
        w_template, h_template = template_processed.shape[::-1]

        norm_array = []

        for i in range(w-w_template):
            for j in range(h-h_template):
                if norm(subimage[j:j+h_template,i:i+w_template] - template_processed, ord='fro')<16:
                    #norm_array.append((i+170,j+170/2))
                    norm_array.append(((i+(int(center[0]))-170),j+(int(center[1])-170)))


        #print int(center[1]-170), int(center[0]-170)
        np.savetxt("norm_array.dat",norm_array,delimiter="")

        return norm_array





    def template_match(self):
        
        start = time.time()
        self.template_centers = []
        currentFrame = cv2.resize(cv2.cvtColor(self.video.currentFrame, cv2.COLOR_BGR2GRAY),(640,480))
        
        self.match_preprocess(currentFrame)
        print "Preprocess time", int((time.time() - start)*1000.0)

        start = time.time()
        norm_array = self.match()

        template_processed = cv2.imread('preprocessed-template.png',cv2.IMREAD_GRAYSCALE)
        w_template, h_template = template_processed.shape[::-1]

        print w_template,h_template

        #print norm_array
        for pt in norm_array:
            if self.template_centers == []:
                #print "Good Point",pt
                self.template_centers.append(pt)
                cv2.rectangle(currentFrame, pt, (pt[0] + w_template, pt[1] + h_template), (255,255,255), 1)
                
            good_point = True
            for i in self.template_centers:
                if abs(i[0] - pt[0])<(w_template*0.7) and abs(i[1] - pt[1])<(h_template*0.7):
                    good_point = False 
            if good_point:
                #print "Good Point", pt
                self.template_centers.append(pt)
                cv2.rectangle(currentFrame, pt, (pt[0] + w_template, pt[1] + h_template), (255,255,255), 1)
                good_point = False 
            


        self.template_centers, dist = kmeans(np.asarray(norm_array),len(self.template_centers),thresh=1e-06)
        self.template_centers = [(i[0] + w_template/2.0, i[1]+h_template/2.0) for i in self.template_centers]
        print "Matching + Kmeans time", int((time.time() - start)*1000.0)



        print "Number of non-overlapping matches are ", len(self.template_centers)
        
        self.template_centers_sorted = []
        self.template_centers_sorted.append(self.template_centers.pop(0))

        i = self.template_centers_sorted[0]

        while(len(self.template_centers)>0):
            dist = 1000000000

            for j in self.template_centers:
                if (pow(j[0]-i[0],2) + pow(j[1]-i[1],2))<dist:
                    dist = pow(j[0]-i[0],2) + pow(j[1]-i[1],2)
                    nn = j
            i = nn
            self.template_centers_sorted.append(self.template_centers.pop(self.template_centers.index(nn)))

	for i in self.template_centers_sorted:
		cv2.circle(currentFrame, (int(i[0]),int(i[1])), 2, 255, thickness=-1)
	cv2.imwrite('res.png',currentFrame)
        
        self.template_centers = self.template_centers_sorted
        
        self.make_template_path()





    def template_match_opencv(self):
        

        self.template_centers = []
        template = cv2.imread('template.png',cv2.IMREAD_GRAYSCALE)
        #plt.imshow(template)
        #plt.show()
        currentFrame = cv2.resize(cv2.cvtColor(self.video.currentFrame, cv2.COLOR_BGR2GRAY),(640,480))
        #currentFrame = cv2.copyMakeBorder(currentFrame,100,100,100,100,cv2.BORDER_CONSTANT,value=255)
        w, h = template.shape[::-1]
    
        res = cv2.matchTemplate(currentFrame,template,cv2.TM_CCOEFF_NORMED)
        threshold = 0.6
        loc = np.where( res >= threshold)
        print "Number of overlapping matches are ", len(loc[0])
        print "Template Size:", w,h
        print loc
        for pt in zip(*loc[::-1]):
            
            if self.template_centers == []:
                self.template_centers.append(pt)
                cv2.rectangle(currentFrame, pt, (pt[0] + w, pt[1] + h), (255,255,255), 1)
                
            good_point = True
            for i in self.template_centers:
                if abs(i[0] - pt[0])<(w*0.5) and abs(i[1] - pt[1])<(h*0.5):
                    good_point = False 
            if good_point:
                print pt
                self.template_centers.append(pt)
                cv2.rectangle(currentFrame, pt, (pt[0] + w, pt[1] + h), (255,255,255), 1)
                good_point = False 

        self.template_centers, dist = kmeans(np.asarray(zip(*loc[::-1])),len(self.template_centers),thresh=1e-06)
        self.template_centers = [(i[0] + w/2.0, i[1]+h/2.0) for i in self.template_centers]

        print "Number of non-overlapping matches are ", len(self.template_centers)
        cv2.imwrite('res.png',currentFrame)
        
        ctr=0
        self.template_centers_sorted = []
        self.template_centers_sorted.append(self.template_centers.pop(0))

        i = self.template_centers_sorted[0]

        while(len(self.template_centers)>0):
            dist = 1000000000

            for j in self.template_centers:
                if (pow(j[0]-i[0],2) + pow(j[1]-i[1],2))<dist:
                    dist = pow(j[0]-i[0],2) + pow(j[1]-i[1],2)
                    nn = j
            i = nn
            self.template_centers_sorted.append(self.template_centers.pop(self.template_centers.index(nn)))


        self.template_centers = self.template_centers_sorted
        
        self.make_template_path()


    def exec_path(self):
        #self.tr_smooth_path()
        self.tr_playback()
 
    def make_template_path(self):
        self.target_points = []
        t=0
        tmp = self.video.aff_matrix
        prevAngle = 0
        for i in self.template_centers:
            drop = 6
            y = (tmp[0][0]*i[0] + tmp[0][1]*i[1]+tmp[0][2])/10.0
            z = (tmp[1][0]*i[0] + tmp[1][1]*i[1]+tmp[1][2])/10.0

            d = sqrt((pow(y,2) + pow(z,2)))
            phi = .0017*pow(d,4)-.08888*pow(d,3) + 1.45674*pow(d,2) - 11.7664*pow(d,1) + 158.496

            pose = (2,y,z,phi)
            joint_angles = self.rex.rexarm_IK(pose,1)
            if (joint_angles != []):
                baseAngle = abs((atan2(z,y) - prevAngle))/(2*PI)
                if (baseAngle*prevAngle <= 0 and (abs(baseAngle) < PI/4) and (abs(prevAngle) <PI/4)):
                    t+=(1000)
                else:
                    t+=(1000*baseAngle)
                self.target_points.append((4,y,z,t))
                t+=150
                self.target_points.append((0,y,z,t))
                t+=150
                self.target_points.append((4,y,z,t))
                t+=150
                prevAngle = atan2(z,y)
        self.target_points.append((41,0,0,t))
        t+=2000
        waypoint_list = []
        prev_joint = (0,0,0,0)
        prev_time = 0
        for member in self.target_points:
            d = sqrt((pow(member[1],2) + pow(member[2],2)))
            phi = .0017*pow(d,4)-.08888*pow(d,3) + 1.45674*pow(d,2) - 11.7664*pow(d,1) + 158.496

            pose = (member[0],member[1],member[2],phi)
            joint_angles = self.rex.rexarm_IK(pose,1)
            time_diff = member[3] - prev_time
            if (joint_angles != [] ):
                speeds = [abs(joint_angles[0]-prev_joint[0])/time_diff,abs(joint_angles[1]-prev_joint[1])/time_diff,
                abs(joint_angles[2]-prev_joint[2])/time_diff,abs(joint_angles[3]-prev_joint[3])/time_diff]
                print pose,speeds
                waypoint_list.append((member[3],joint_angles[0],joint_angles[1],
                    joint_angles[2],joint_angles[3],speeds[0],speeds[1],speeds[2],speeds[3]))
                prev_joint = joint_angles
                prev_time = member[3]       # print waypoint_list
        waypoint_list.append((member[3],0,0,0,0,speeds[0],speeds[1],speeds[2],speeds[3]))
        tmp = np.asarray(waypoint_list)
        np.savetxt('waypoint_list.dat',tmp, delimiter=',')

def main():
    app = QtGui.QApplication(sys.argv)
    ex = Gui()
    ex.show()
    sys.exit(app.exec_())
 
if __name__ == '__main__':
    main()
